import os
import pandas as pd
import re


def lemmatize(line):
    voc = pd.read_csv('voc.csv', sep = '\t').sort_values(by='permil', ascending=False)
    linewords = []
    for word in line.split():
        wl = voc[voc['sort'] == word]
        if len(set(wl['lex']))>=1:
            linewords.append(list(wl['lex'])[0])
        else:
            linewords.append(word)
    text_spl = (' '.join(linewords))
    return text_spl